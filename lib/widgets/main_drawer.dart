import 'package:flutter/material.dart';
import 'package:foot_app/helper/shourtcut.dart';
import 'package:foot_app/screen/filtter_screen.dart';

class MainDrawer extends StatelessWidget {
  const MainDrawer({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          Container(
            color: Theme.of(context).primaryColor,
            alignment: Alignment.centerLeft,
            height: 200,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            child: Text(
              'Cooking Up!',
              style: TextStyle(
                fontSize: 35,
                fontWeight: FontWeight.w900,
                color: Theme.of(context).accentColor,
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          buildListTile(
            'Meals',
            Icons.restaurant,
            () {
              Navigator.of(context).pushReplacementNamed('/');
            },
          ),
          buildListTile(
            'Filtters',
            Icons.settings,
            () {
              Navigator.of(context).pushReplacementNamed(FiltterScreen.route);
            },
          ),
        ],
      ),
    );
  }
}
