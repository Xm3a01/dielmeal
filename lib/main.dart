import 'package:flutter/material.dart';
import './models/dummy_category.dart';
import './models/meal.dart';
import './screen/filtter_screen.dart';
import './screen/tabBar_screen.dart';
import './screen/meal_detials.dart';
import './screen/category_meals_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Map<String, bool> _filtters = {
    'gluten': false,
    'vegan': false,
    'vegeterian': false,
    'lactose': false
  };

  List<Meal> _availabeMeals = DUMMY_MEALS;

  void _saveFiltter(Map<String, bool> sFiltter) {
    setState(() {
      _filtters = sFiltter;
      _availabeMeals = DUMMY_MEALS.where((meal) {
        if (_filtters['gluten'] && !meal.isGlutenFree) {
          return false;
        }
        if (_filtters['vegan'] && !meal.isVegan) {
          return false;
        }
        if (_filtters['vegeterian'] && !meal.isVegetarian) {
          return false;
        }
        if (_filtters['lactose'] && !meal.isLactoseFree) {
          return false;
        }
        return true;
      }).toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.green,
        accentColor: Colors.amber,
        canvasColor: Colors.grey[300],
        fontFamily: 'Raleway',
        textTheme: ThemeData.light().textTheme.copyWith(
              bodyText1: const TextStyle(
                color: Color.fromRGBO(20, 51, 51, 1),
              ),
              bodyText2: const TextStyle(
                color: Color.fromRGBO(20, 51, 51, 1),
              ),
              headline6: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      // home: CategoryScreen(),
      initialRoute: TabsBarScreen.route,
      routes: {
        TabsBarScreen.route: (context) => TabsBarScreen(),
        CategoryMealsScreen.route: (context) =>
            CategoryMealsScreen(_availabeMeals),
        MealDetials.route: (context) => MealDetials(),
        FiltterScreen.route: (context) => FiltterScreen(_filtters, _saveFiltter)
      },
    );
  }
}
