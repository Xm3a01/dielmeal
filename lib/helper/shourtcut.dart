import 'package:flutter/material.dart';

Widget titleContainer(BuildContext context, String text) {
  return Container(
    margin: EdgeInsets.symmetric(vertical: 10),
    child: Text(
      text,
      style: Theme.of(context).textTheme.headline6,
    ),
  );
}

Widget contentContainer(Widget child) {
  return Container(
    padding: EdgeInsets.all(10),
    decoration: BoxDecoration(
      border: Border.all(width: 1, color: Colors.grey),
      borderRadius: BorderRadius.circular(10),
    ),
    height: 200,
    width: 300,
    child: child,
  );
}

Widget buildListTile(String title, IconData icon, Function gotContent) {
  return ListTile(
    onTap: gotContent,
    leading: Icon(
      icon,
      size: 30,
    ),
    title: Text(
      title,
      style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
    ),
  );
}

Widget buildSwitchTitle(
    {String title, String description, bool value, Function setValue}) {
  return SwitchListTile(
      title: Text(title),
      subtitle: Text(description),
      value: value,
      onChanged: (newValue) => setValue(newValue));
}
