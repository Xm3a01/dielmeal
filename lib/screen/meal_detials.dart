import 'package:flutter/material.dart';
import '../helper/shourtcut.dart';
import '../models/dummy_category.dart';

class MealDetials extends StatelessWidget {
  static const route = '/meal-detials';
  const MealDetials({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final mealId = ModalRoute.of(context).settings.arguments as String;
    final meal = DUMMY_MEALS.firstWhere((m) {
      return m.id == mealId;
    });
    return Scaffold(
      appBar: AppBar(
        title: Text(meal.title),
      ),
      body: Column(
        children: [
          Container(
            height: 300,
            width: double.infinity,
            child: Image.network(meal.imageUrl),
          ),
          titleContainer(context, 'Ingredinate'),
          contentContainer(
            ListView.builder(
              itemBuilder: (ctx, index) => Card(
                color: Theme.of(context).accentColor,
                child: Padding(
                  padding: const EdgeInsets.all(5),
                  child: Text(
                    meal.ingredients[index],
                  ),
                ),
              ),
              itemCount: meal.ingredients.length,
            ),
          ),
          titleContainer(context, 'Steps'),
          contentContainer(
            ListView.builder(
              itemBuilder: (ctx, index) => ListTile(
                leading: CircleAvatar(
                  child: Text('#${(index + 1)}'),
                ),
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('${meal.steps[index]}'),
                    Divider(),
                  ],
                ),
              ),
              itemCount: meal.steps.length,
            ),
          ),
        ],
      ),
    );
  }
}
