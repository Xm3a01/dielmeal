import 'package:flutter/material.dart';
import '../helper/shourtcut.dart';
import '../widgets/main_drawer.dart';

class FiltterScreen extends StatefulWidget {
  static const route = '/filtter-screen';

  final Function saveFiltter;
  final Map<String, bool> currentFiltters;

  const FiltterScreen(this.currentFiltters, this.saveFiltter) : super();

  @override
  _FiltterScreenState createState() => _FiltterScreenState();
}

class _FiltterScreenState extends State<FiltterScreen> {
  bool _glutenFree = false;
  bool _vegetarian = false;
  bool _vegan = false;
  bool _lactoseFree = false;

  @override
  void initState() {
    _glutenFree = widget.currentFiltters['gluten'];
    _vegetarian = widget.currentFiltters['vegeterian'];
    _vegan = widget.currentFiltters['vegan'];
    _lactoseFree = widget.currentFiltters['lactose'];

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MainDrawer(),
      appBar: AppBar(
        title: Text('Fitter Screen'),
        actions: [
          IconButton(
            icon: Icon(Icons.save),
            onPressed: () {
              widget.saveFiltter({
                'gluten': _glutenFree,
                'vegeterian': _vegetarian,
                'vegan': _vegan,
                'lactose': _lactoseFree
              });
            },
          )
        ],
      ),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.all(20),
            child: Text(
              'Adjust Fitter Setting',
              style: Theme.of(context).textTheme.headline6,
            ),
          ),
          Expanded(
            child: ListView(children: [
              buildSwitchTitle(
                  title: 'Glutent-free',
                  description: 'Meal free from gluatien',
                  value: _glutenFree,
                  setValue: (newValue) {
                    setState(() {
                      _glutenFree = newValue;
                    });
                  }),
              buildSwitchTitle(
                  title: 'Vegetarian',
                  description: 'Meal content sutable for Vegetarian',
                  value: _vegetarian,
                  setValue: (newValue) {
                    setState(() {
                      _vegetarian = newValue;
                    });
                  }),
              buildSwitchTitle(
                  title: 'Vegan',
                  description: 'Meal is vegan',
                  value: _vegan,
                  setValue: (newValue) {
                    setState(() {
                      _vegan = newValue;
                    });
                  }),
              buildSwitchTitle(
                  title: 'Lactose-Free',
                  description: 'Meal free from gluatien',
                  value: _lactoseFree,
                  setValue: (newValue) {
                    setState(() {
                      _lactoseFree = newValue;
                    });
                  }),
            ]),
          ),
        ],
      ),
    );
  }
}
