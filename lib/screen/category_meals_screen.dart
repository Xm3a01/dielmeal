import 'package:flutter/material.dart';
import '../models/meal.dart';
import '../widgets/meals_item.dart';

class CategoryMealsScreen extends StatelessWidget {
  static const route = '/category-meals';

  final List<Meal> availableMeals;

  CategoryMealsScreen(this.availableMeals);

  @override
  Widget build(BuildContext context) {
    final argument =
        ModalRoute.of(context).settings.arguments as Map<String, String>;
    final meals = availableMeals.where((meal) {
      return meal.categories.contains(argument['id']);
    }).toList();
    return Scaffold(
      // backgroundColor: Colors.grey[400],
      appBar: AppBar(
        title: Text(argument['title']),
      ),
      body: ListView.builder(
        itemBuilder: (ctx, index) {
          return MealsItem(
            id: meals[index].id,
            title: meals[index].title,
            duration: meals[index].duration,
            imageUrl: meals[index].imageUrl,
            complexity: meals[index].complexity,
            affordability: meals[index].affordability,
          );
        },
        itemCount: meals.length,
      ),
    );
  }
}
